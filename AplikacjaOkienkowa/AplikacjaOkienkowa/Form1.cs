﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Windows.Forms.DataVisualization.Charting;

namespace AplikacjaOkienkowa
{
    public partial class Form1 : Form
    {
        string sSource = "Graph Chart App";
        string sLog = "Application";
        string sEvent;

        private static int PHYSICAL = 23;
        private static int EMOTIONAL = 28;
        private static int INTELECTUAL = 33;

        private static int GRAPH_LINE_THICKNESS = 1;

        private static DateTime pickedDate = DateTime.Now;
        private static DateTime now = DateTime.Now;

        // app stuff

        public Form1()
        {
            InitializeComponent();
            graphChart.Series.Clear();
            graphChart.Legends.Clear();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void clearCurrentChart()
        {
            // clearing old chart
            graphChart.Series.Clear();
            graphChart.Legends.Clear();
        }

        private void chartDeclarations()
        {
            // setting graph values interval
            graphChart.ChartAreas[0].AxisX.Interval = 1;
            graphChart.ChartAreas[0].AxisY.Interval = 25;

            // setting maximum and minimum values for X
            graphChart.ChartAreas[0].AxisX.Maximum = 28;
            graphChart.ChartAreas[0].AxisX.Minimum = 1;

            // setting maximum and minimum values for Y
            graphChart.ChartAreas[0].AxisY.Maximum = 100;
            graphChart.ChartAreas[0].AxisY.Minimum = -100;
        }

        private void seriesDeclarations()
        {
            graphChart.Series.Add(generateSerie("Intelectual", INTELECTUAL));
            graphChart.Series["Intelectual"].ChartType = SeriesChartType.Line;
            graphChart.Legends.Add(new Legend("Intelectual"));
            graphChart.Series["Intelectual"].Color = Color.Red;
            graphChart.Series["Intelectual"].BorderWidth = GRAPH_LINE_THICKNESS;

            graphChart.Series.Add(generateSerie("Physical", PHYSICAL));
            graphChart.Series["Physical"].ChartType = SeriesChartType.Line;
            graphChart.Legends.Add(new Legend("Physical"));
            graphChart.Series["Physical"].Color = Color.Green;
            graphChart.Series["Physical"].BorderWidth = GRAPH_LINE_THICKNESS;

            graphChart.Series.Add(generateSerie("Emotional", EMOTIONAL));
            graphChart.Series["Emotional"].ChartType = SeriesChartType.Line;
            graphChart.Legends.Add(new Legend("Emotional"));
            graphChart.Series["Emotional"].Color = Color.Blue;
            graphChart.Series["Emotional"].BorderWidth = GRAPH_LINE_THICKNESS;
        }

        private void drawButton_Click(object sender, EventArgs e)
        {

            sEvent = "Button has been clicked, drawing charts.";
            EventLog.WriteEntry(sLog, sEvent);

            clearCurrentChart();

            chartDeclarations();

            seriesDeclarations();

        }

        private static Series generateSerie(string name, int type)
        {
            Series series = new Series(name);

            int daysSinceBorn = (now - pickedDate).Days;

            for (int dayOfLife = daysSinceBorn - 14; dayOfLife < daysSinceBorn + 14; dayOfLife++)
            {
                series.Points.AddXY(pickedDate.AddDays(dayOfLife).ToShortDateString(), getBiorythmValue(dayOfLife, type));
            }

            return series;
        }

        private static double getBiorythmValue(int dayOfLife, int type)
        {
            return Math.Sin(2 * Math.PI * dayOfLife / type) * 100;
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            pickedDate = dateTimePicker.Value;
        }

    }
}
