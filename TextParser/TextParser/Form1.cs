﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace TextParser
{
    public partial class Form1 : Form
    {

        private DirectoryInfo directoryInformation;
        private FileInfo[] filesInformation;

        private string filesLocation = Path.Combine(Environment.CurrentDirectory, "..\\..\\files");
        private string selectedFilePath;

        public Form1()
        {
            InitializeComponent();
            this.saveButton.Enabled = false;
            directoryInformation = new DirectoryInfo(this.filesLocation);
            updateFilesList();
        }

        private void updateFilesList()
        {
            filesList.Items.Clear();
            filesInformation = directoryInformation.GetFiles("*.txt");
            foreach (FileInfo fileFound in filesInformation)
            {
                filesList.Items.Add(fileFound.Name);
            }
        }

        private void filesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.originalTextBox.Clear();
            selectedFilePath = filesInformation[filesList.SelectedIndex].FullName;

            if (File.Exists(selectedFilePath)) readFile(selectedFilePath);
        }

        private void originalTextBox_TextChanged(object sender, EventArgs e)
        {
            this.modifiedTextBox.Clear();
            this.saveButton.Enabled = true;
        }



        private void readFile(string filePath)
        {
            originalTextBox.AppendText(File.ReadAllText(filePath));
            parseWords(filePath);
        }

        private void parseWords(string filePath)
        {
            string text = File.ReadAllText(filePath);
            foreach (string line in File.ReadAllLines(filePath))
            {
                foreach (string word in line.Split(' '))
                {
                    text = text.Replace(word, checkForModifications(word));
                }
            }
            modifiedTextBox.Text = text;
        }

        private string checkForModifications(string word)
        {
            if (word.ToLower().StartsWith("do"))
            {
                return word.ToUpper();
            }
            else if (word.ToLower().EndsWith("us"))
            {
                return (word.ToLower().First().ToString().ToUpper()) + word.ToLower().Substring(1);
            }
            else if (sOdd(word))
            {
                return reverseWord(word);
            }

            return word;
        }

        private bool sOdd(string word)
        {
            int count = 0;
            foreach (char c in word.ToLower())
            {
                if (c == 's') count++;
            }
            if (count % 2 == 1) return true;
            return false;
        }

        private string reverseWord(string word)
        {
            char[] letters = word.ToCharArray();
            Array.Reverse(letters);
            return new string(letters);
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            string newFilePath = (selectedFilePath.Substring(0, selectedFilePath.Length - 4) + "_WYNIKOWY.txt");
            File.WriteAllText(newFilePath, this.modifiedTextBox.Text);
            updateFilesList();
        }
    }
}
