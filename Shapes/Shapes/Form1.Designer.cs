﻿namespace Shapes
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.shapesList = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.newShapesLabel = new System.Windows.Forms.Label();
            this.newTriangleButton = new System.Windows.Forms.Button();
            this.newRectangleButton = new System.Windows.Forms.Button();
            this.newEllipseButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.deleteShapeButton = new System.Windows.Forms.Button();
            this.sortByCircumferenceButton = new System.Windows.Forms.Button();
            this.sortByAreaButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.circumferenceLabel = new System.Windows.Forms.Label();
            this.circumferenceValue = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.areaLabel = new System.Windows.Forms.Label();
            this.areaValueLabel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.Controls.Add(this.shapesList, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(560, 338);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // shapesList
            // 
            this.shapesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shapesList.FormattingEnabled = true;
            this.shapesList.Location = new System.Drawing.Point(143, 3);
            this.shapesList.Name = "shapesList";
            this.shapesList.Size = new System.Drawing.Size(274, 332);
            this.shapesList.TabIndex = 0;
            this.shapesList.SelectedIndexChanged += new System.EventHandler(this.shapesList_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.newShapesLabel, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.newTriangleButton, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.newRectangleButton, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.newEllipseButton, 0, 3);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(134, 332);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // newShapesLabel
            // 
            this.newShapesLabel.AutoSize = true;
            this.newShapesLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newShapesLabel.Font = new System.Drawing.Font("Lucida Console", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.newShapesLabel.Location = new System.Drawing.Point(3, 0);
            this.newShapesLabel.Name = "newShapesLabel";
            this.newShapesLabel.Size = new System.Drawing.Size(128, 33);
            this.newShapesLabel.TabIndex = 0;
            this.newShapesLabel.Text = "New";
            this.newShapesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // newTriangleButton
            // 
            this.newTriangleButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newTriangleButton.Image = ((System.Drawing.Image)(resources.GetObject("newTriangleButton.Image")));
            this.newTriangleButton.Location = new System.Drawing.Point(3, 36);
            this.newTriangleButton.Name = "newTriangleButton";
            this.newTriangleButton.Size = new System.Drawing.Size(128, 93);
            this.newTriangleButton.TabIndex = 1;
            this.newTriangleButton.UseVisualStyleBackColor = true;
            this.newTriangleButton.Click += new System.EventHandler(this.newTriangleButton_Click);
            // 
            // newRectangleButton
            // 
            this.newRectangleButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newRectangleButton.Image = ((System.Drawing.Image)(resources.GetObject("newRectangleButton.Image")));
            this.newRectangleButton.Location = new System.Drawing.Point(3, 135);
            this.newRectangleButton.Name = "newRectangleButton";
            this.newRectangleButton.Size = new System.Drawing.Size(128, 93);
            this.newRectangleButton.TabIndex = 2;
            this.newRectangleButton.UseVisualStyleBackColor = true;
            this.newRectangleButton.Click += new System.EventHandler(this.newRectangleButton_Click);
            // 
            // newEllipseButton
            // 
            this.newEllipseButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.newEllipseButton.Image = ((System.Drawing.Image)(resources.GetObject("newEllipseButton.Image")));
            this.newEllipseButton.Location = new System.Drawing.Point(3, 234);
            this.newEllipseButton.Name = "newEllipseButton";
            this.newEllipseButton.Size = new System.Drawing.Size(128, 95);
            this.newEllipseButton.TabIndex = 3;
            this.newEllipseButton.UseVisualStyleBackColor = true;
            this.newEllipseButton.Click += new System.EventHandler(this.newEllipseButton_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.deleteShapeButton, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.sortByCircumferenceButton, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.sortByAreaButton, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 4);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(423, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 5;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(134, 332);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // deleteShapeButton
            // 
            this.deleteShapeButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.deleteShapeButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteShapeButton.Image")));
            this.deleteShapeButton.Location = new System.Drawing.Point(3, 3);
            this.deleteShapeButton.Name = "deleteShapeButton";
            this.deleteShapeButton.Size = new System.Drawing.Size(128, 60);
            this.deleteShapeButton.TabIndex = 0;
            this.deleteShapeButton.UseVisualStyleBackColor = true;
            this.deleteShapeButton.Click += new System.EventHandler(this.deleteShapeButton_Click);
            // 
            // sortByCircumferenceButton
            // 
            this.sortByCircumferenceButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sortByCircumferenceButton.Image = ((System.Drawing.Image)(resources.GetObject("sortByCircumferenceButton.Image")));
            this.sortByCircumferenceButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sortByCircumferenceButton.Location = new System.Drawing.Point(3, 69);
            this.sortByCircumferenceButton.Name = "sortByCircumferenceButton";
            this.sortByCircumferenceButton.Size = new System.Drawing.Size(128, 60);
            this.sortByCircumferenceButton.TabIndex = 1;
            this.sortByCircumferenceButton.Text = "Circumference";
            this.sortByCircumferenceButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.sortByCircumferenceButton.UseVisualStyleBackColor = true;
            this.sortByCircumferenceButton.Click += new System.EventHandler(this.sortByCircumferenceButton_Click);
            // 
            // sortByAreaButton
            // 
            this.sortByAreaButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sortByAreaButton.Image = ((System.Drawing.Image)(resources.GetObject("sortByAreaButton.Image")));
            this.sortByAreaButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.sortByAreaButton.Location = new System.Drawing.Point(3, 135);
            this.sortByAreaButton.Name = "sortByAreaButton";
            this.sortByAreaButton.Size = new System.Drawing.Size(128, 60);
            this.sortByAreaButton.TabIndex = 2;
            this.sortByAreaButton.Text = "Area";
            this.sortByAreaButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.sortByAreaButton.UseVisualStyleBackColor = true;
            this.sortByAreaButton.Click += new System.EventHandler(this.sortByAreaButton_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.circumferenceLabel, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.circumferenceValue, 0, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 201);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(128, 60);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // circumferenceLabel
            // 
            this.circumferenceLabel.AutoSize = true;
            this.circumferenceLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.circumferenceLabel.Location = new System.Drawing.Point(3, 0);
            this.circumferenceLabel.Name = "circumferenceLabel";
            this.circumferenceLabel.Size = new System.Drawing.Size(122, 30);
            this.circumferenceLabel.TabIndex = 0;
            this.circumferenceLabel.Text = "Circumference";
            this.circumferenceLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // circumferenceValue
            // 
            this.circumferenceValue.AutoSize = true;
            this.circumferenceValue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.circumferenceValue.Location = new System.Drawing.Point(3, 30);
            this.circumferenceValue.Name = "circumferenceValue";
            this.circumferenceValue.Size = new System.Drawing.Size(122, 30);
            this.circumferenceValue.TabIndex = 1;
            this.circumferenceValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.areaLabel, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.areaValueLabel, 0, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 267);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(128, 62);
            this.tableLayoutPanel5.TabIndex = 4;
            // 
            // areaLabel
            // 
            this.areaLabel.AutoSize = true;
            this.areaLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.areaLabel.Location = new System.Drawing.Point(3, 0);
            this.areaLabel.Name = "areaLabel";
            this.areaLabel.Size = new System.Drawing.Size(122, 31);
            this.areaLabel.TabIndex = 0;
            this.areaLabel.Text = "Area";
            this.areaLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // areaValueLabel
            // 
            this.areaValueLabel.AutoSize = true;
            this.areaValueLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.areaValueLabel.Location = new System.Drawing.Point(3, 31);
            this.areaValueLabel.Name = "areaValueLabel";
            this.areaValueLabel.Size = new System.Drawing.Size(122, 31);
            this.areaValueLabel.TabIndex = 1;
            this.areaValueLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 362);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ListBox shapesList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label newShapesLabel;
        private System.Windows.Forms.Button newTriangleButton;
        private System.Windows.Forms.Button newRectangleButton;
        private System.Windows.Forms.Button newEllipseButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button deleteShapeButton;
        private System.Windows.Forms.Button sortByCircumferenceButton;
        private System.Windows.Forms.Button sortByAreaButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label circumferenceLabel;
        private System.Windows.Forms.Label circumferenceValue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label areaLabel;
        private System.Windows.Forms.Label areaValueLabel;
    }
}

