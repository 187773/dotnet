﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Shapes
{
    public partial class Form1 : Form
    {
        bool areaAsc = true;
        bool circumferenceAsc = true;
        Random rng = new Random();
        private string filesLocation = Path.Combine(Environment.CurrentDirectory, "..\\..\\images");

        public Form1()
        {
            InitializeComponent();
            setUpComponents();
        }

        private void setUpComponents()
        {
            sortByAreaButton.Text = "Area";
            sortByCircumferenceButton.Text = "Circumference";
            unlockButtons(false);
        }

        private void shapesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (shapesList.SelectedIndex < 0) return;
            Object selectedObject = shapesList.SelectedItem;
            IShape shape = null;
            shape = (IShape)selectedObject;
            circumferenceValue.Text = shape.getCircumference().ToString("#.000");
            areaValueLabel.Text = shape.getArea().ToString("#.000");

        }

        private void unlockButtons(bool value)
        {
            deleteShapeButton.Enabled = value;
            sortByAreaButton.Enabled = value;
            sortByCircumferenceButton.Enabled = value;
        }

        private void newTriangleButton_Click(object sender, EventArgs e)
        {
            shapesList.Items.Add(new MyTriangle(rng.Next(1, 10), rng.Next(1, 10)));
            unlockButtons(true);
        }

        private void newRectangleButton_Click(object sender, EventArgs e)
        {
            shapesList.Items.Add(new MyRectangle(rng.Next(1, 10), rng.Next(1, 10)));
            unlockButtons(true);
        }

        private void newEllipseButton_Click(object sender, EventArgs e)
        {
            shapesList.Items.Add(new MyEllipse(rng.Next(1, 10), rng.Next(1, 10)));
            unlockButtons(true);
        }

        private void deleteShapeButton_Click(object sender, EventArgs e)
        {
			int currentIndex = shapesList.SelectedIndex;
            if (shapesList.Items.Count > 0)
            {
                if (shapesList.SelectedIndex < 0) shapesList.SetSelected(0, true);
                shapesList.Items.RemoveAt(shapesList.SelectedIndex);
                if (shapesList.Items.Count == 0)
                {
                    unlockButtons(false);
                    circumferenceValue.Text = "";
                    areaValueLabel.Text = "";
                }

            }
            if (deleteShapeButton.Enabled.Equals(true))
            {
                if (shapesList.Items.Count <= currentIndex) currentIndex--;
				shapesList.SetSelected(currentIndex, true);
            }
        }

        private void sortByCircumference(bool asc)
        {
            Dictionary<IShape, double> elements = new Dictionary<IShape, double>();
            foreach (IShape element in shapesList.Items)
            {
                elements.Add(element, element.getCircumference());
            }
            if (circumferenceAsc)
            {
                elements = elements.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            } else
            {
                elements = elements.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            }
            shapesList.Items.Clear();
            foreach (IShape key in elements.Keys) {
                shapesList.Items.Add(key);
            }
        }

        private void sortByCircumferenceButton_Click(object sender, EventArgs e)
        {
            sortByAreaButton.Image = Image.FromFile(Path.Combine(filesLocation, "arrows.png"));
            areaAsc = false;
            sortByCircumference(circumferenceAsc);
            if (circumferenceAsc)
            {
                sortByCircumferenceButton.Image = Image.FromFile(Path.Combine(filesLocation, "uparr.png"));
                circumferenceAsc = false;
            } else
            {
                sortByCircumferenceButton.Image = Image.FromFile(Path.Combine(filesLocation, "downarr.png"));
                circumferenceAsc = true;
            }
            shapesList.SetSelected(0, true);

        }

        private void sortByArea(bool asc)
        {
            Dictionary<IShape, double> elements = new Dictionary<IShape, double>();
            foreach (IShape element in shapesList.Items)
            {
                elements.Add(element, element.getArea());
            }
            if (areaAsc)
            {
                elements = elements.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            }
            else
            {
                elements = elements.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            }
            shapesList.Items.Clear();
            foreach (IShape key in elements.Keys)
            {
                shapesList.Items.Add(key);
            }
        }

        private void sortByAreaButton_Click(object sender, EventArgs e)
        {
            sortByCircumferenceButton.Image = Image.FromFile(Path.Combine(filesLocation, "arrows.png"));
            circumferenceAsc = true;
            sortByArea(areaAsc);
            if (areaAsc)
            {
                sortByAreaButton.Image = Image.FromFile(Path.Combine(filesLocation, "uparr.png"));
                areaAsc = false;
            }
            else
            {
                sortByAreaButton.Image = Image.FromFile(Path.Combine(filesLocation, "downarr.png"));
                areaAsc = true;
            }
            shapesList.SetSelected(0, true);
        }
    }

    interface IShape
    {

        double getArea();
        double getCircumference();
    }

    class MyTriangle : IShape
    {
        public MyTriangle(int givenA, int givenB)
        {
            a = givenA;
            b = givenB;
        }

        private int a;
        private int b;

        public double getArea()
        {
            return ((0.5 * a) * b);
        }

        public double getCircumference()
        {
            return (Math.Sqrt(Math.Pow(a,2) + Math.Pow(b,2)) + a + b);
        }

        public override string ToString()
        {
            return "TRIANGLE; " + a + ", " + b;
        }
    }

    class MyRectangle : IShape
    {
        public MyRectangle(int givenA, int givenB)
        {
            a = givenA;
            b = givenB;
        }

        private int a;
        private int b;

        public double getArea()
        {
            return (a * b);
        }

        public double getCircumference()
        {
            return (2 * a + 2 * b);
        }

        public override string ToString()
        {
            return "RECTANGLE; " + a + ", " + b;
        }
    }

    class MyEllipse : IShape
    {
        public MyEllipse(int givenA, int givenB)
        {
            a = givenA;
            b = givenB;
        }

        private int a;
        private int b;

        public double getArea()
        {
            return (Math.PI * a * b);
        }

        public double getCircumference()
        {
            // using simplified equation
            return (Math.PI * (1.5 * (a + b) - Math.Sqrt(a * b)));
        }

        public override string ToString()
        {
            return "ELLIPSE; " + a + ", " + b;
        }
    }
}
