﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace Threads
{
    public partial class Form1 : Form
    {
        Thread waveThread;
        DateTime tick;
        DateTime tock;

        public Form1()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            waveThread = new Thread(new ThreadStart(this.start));
            waveThread.Start();
        }

        private void FormClosingEvent(object sender, FormClosingEventArgs e)
        {
            waveThread.Abort();
        }

        Color color;
        double t;

        public void compute(int width, int height)
        {
            double x = width;
            double y = height;

            double a = Math.Pow(x - (pictureBox1.Width / 2), 2);
            double b = Math.Pow(y - (pictureBox1.Height / 2), 2);

            double c = (((a + b) / 1000) + this.t);
            this.color = Color.FromArgb(Convert.ToInt32((0.5 + (0.5 * Math.Sin(c))) * 255), 0, 0);
        }

        private void draw()
        {
            int heightIterator = 0;
            int widthIterator = 0;
            Bitmap bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            tick = DateTime.Now;

            while (widthIterator < bm.Width)
            {
                heightIterator = 0;
                while (heightIterator < bm.Height)
                {
                    compute(widthIterator, heightIterator);
                    bm.SetPixel(widthIterator, heightIterator++, this.color);
                    
                }
                widthIterator++;
            }
            pictureBox1.Image = bm;
            tock = DateTime.Now;
        }

        public void start()
        {
            this.t = 0.0;
            
            while (true)
            {
                draw();
                this.Text = (Math.Floor(t * 10)).ToString();
                //Console.WriteLine("Elapsed: " + (tock - tick).TotalSeconds);
                t += (tock - tick).TotalSeconds;
                //t += 0.1;

                //Thread.Sleep(50);
            }
        }
    }
}
