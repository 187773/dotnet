﻿namespace Subtitles
{
    partial class formTitle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.sourceText = new System.Windows.Forms.TextBox();
            this.destText = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.filesList = new System.Windows.Forms.ListBox();
            this.framesController = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framesController)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.Controls.Add(this.sourceText, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.destText, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.saveButton, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.filesList, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.framesController, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 90F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(884, 561);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // sourceText
            // 
            this.sourceText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sourceText.Location = new System.Drawing.Point(179, 3);
            this.sourceText.Multiline = true;
            this.sourceText.Name = "sourceText";
            this.sourceText.ReadOnly = true;
            this.sourceText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.sourceText.Size = new System.Drawing.Size(347, 498);
            this.sourceText.TabIndex = 1;
            // 
            // destText
            // 
            this.destText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.destText.Location = new System.Drawing.Point(532, 3);
            this.destText.Multiline = true;
            this.destText.Name = "destText";
            this.destText.ReadOnly = true;
            this.destText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.destText.Size = new System.Drawing.Size(349, 498);
            this.destText.TabIndex = 2;
            // 
            // saveButton
            // 
            this.saveButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saveButton.Location = new System.Drawing.Point(532, 507);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(349, 51);
            this.saveButton.TabIndex = 4;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // filesList
            // 
            this.filesList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.filesList.FormattingEnabled = true;
            this.filesList.Location = new System.Drawing.Point(3, 3);
            this.filesList.Name = "filesList";
            this.filesList.Size = new System.Drawing.Size(170, 498);
            this.filesList.TabIndex = 5;
            this.filesList.SelectedIndexChanged += new System.EventHandler(this.filesList_SelectedIndexChanged_1);
            // 
            // framesController
            // 
            this.framesController.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.framesController.Location = new System.Drawing.Point(299, 522);
            this.framesController.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.framesController.Minimum = new decimal(new int[] {
            500,
            0,
            0,
            -2147483648});
            this.framesController.Name = "framesController";
            this.framesController.Size = new System.Drawing.Size(107, 20);
            this.framesController.TabIndex = 3;
            this.framesController.ValueChanged += new System.EventHandler(this.framesController_ValueChanged);
            // 
            // formTitle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "formTitle";
            this.Text = "Subtitles editor";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framesController)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox sourceText;
        private System.Windows.Forms.TextBox destText;
        private System.Windows.Forms.NumericUpDown framesController;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.ListBox filesList;
    }
}

