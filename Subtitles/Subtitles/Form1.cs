﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Subtitles
{
    public partial class formTitle : Form
    {
        private DirectoryInfo directoryInformation;
        private FileInfo[] filesInformation;

        private string filesLocation = Path.Combine(Environment.CurrentDirectory, ".");
        private string selectedFilePath;

        int lastSubtitleEnd = -1;
        int currentSubtitleBegin = -1;

        public formTitle()
        {
            InitializeComponent();
            this.saveButton.Enabled = false;
            directoryInformation = new DirectoryInfo(this.filesLocation);
            updateFilesList();
        }

        private void updateFilesList()
        {
            filesList.Items.Clear();
            filesInformation = directoryInformation.GetFiles("*.txt");
            foreach (FileInfo fileFound in filesInformation)
            {
                filesList.Items.Add(fileFound.Name);
            }
        }

        private void readFile(string filePath)
        {
            // fill source text
            this.sourceText.AppendText(File.ReadAllText(filePath));

            // fill dest text
            fillDestText(filePath);
        }

        private void filesList_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            this.sourceText.Clear();
            selectedFilePath = filesInformation[filesList.SelectedIndex].FullName;

            // if selected file exists - open it
            if (File.Exists(selectedFilePath)) readFile(selectedFilePath);
        }

        private void fillDestText(string filePath)
        {            
            // fill dest text from currently selected
            destText.Text = sourceText.Text;

            // update text in Dest (according to up-down controller)
            updateDestText();
        }

        private void updateDestText()
        {
            this.currentSubtitleBegin = -1;
            this.lastSubtitleEnd = -1;
            // empty current text
            destText.Text = "";
            string output = "";
            foreach (string line in sourceText.Lines)
            {                
                if (line.Equals("")) break;
                output = output + (parseLine(line) + Environment.NewLine);
            }
            destText.AppendText(output);
            saveButton.Enabled = true;
        }

        private string parseLine(string line)
        {
            
            int offset = 1;
            string framesFrom;
            string framesTo;
            string lineCopy = line;

            framesFrom = line.Substring(line.IndexOf("{") + offset, line.IndexOf("}") - offset);
            currentSubtitleBegin = Int32.Parse(framesFrom);
            if (currentSubtitleBegin < lastSubtitleEnd) MessageBox.Show("Problem at line:\n" +
                                                                        line +
                                                                        "\nline shows at frame " +
                                                                        currentSubtitleBegin +
                                                                        " while last dissapears at: " +
                                                                        lastSubtitleEnd);
            line = line.Substring(framesFrom.Length);
            
            framesTo = line.Substring(line.IndexOf("{"));
            framesTo = framesTo.Substring(offset, framesTo.IndexOf("}") - offset);
            lastSubtitleEnd = Int32.Parse(framesTo);
            lineCopy = lineCopy.Replace("{" + framesFrom + "}{" + framesTo + "}", "{" + getNewValue(framesFrom) + "}{" + getNewValue(framesTo) + "}");

            return lineCopy;
        }

        private string getNewValue(string val)
        {
            int controllerVar = Decimal.ToInt32(framesController.Value);
            if (Int32.Parse(val) + controllerVar < 0)
            {
                controllerVar = 0 - Int32.Parse(val);
            }
            return (Int32.Parse(val) + controllerVar).ToString();
        }

        private void framesController_ValueChanged(object sender, EventArgs e)
        {
            updateDestText();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            string newFilePath = (selectedFilePath.Substring(0, selectedFilePath.Length - 4) + "_" + framesController.Value.ToString() + ".txt");
            File.WriteAllText(newFilePath, this.destText.Text);
            updateFilesList();
        }

    }
}
